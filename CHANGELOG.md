## [0.3.1] - 2022-05-01

### Fixed

* Fixed an issue where the `new_singleton` macro could require inclusion of the `singleton_trait` crate.

## [0.3.0] - 2021-08-29

### Added

* Overall improved documentation and examples
* New method for the `new_singleton` macro allows one to immediately create an Erased copy.

### Changed

* Upgraded `singleton_trait` to 0.3.0, including improved methods and safety for `Erased`

## [0.2.0] - 2021-08-01

### Added

* Added a `new_singleton` macro which can be used to safely generate singletons for construction in `main`

### Changed

* Upgraded `singleton-trait` to 0.2.0, improving lifetime flexibility

## [0.1.1] - 2021-07-17

### Fixed

Fixed a whitespace issue with the README for this repository, which is most certainly not a repeat issue.

## [0.1.0] - 2021-07-17

### Added

Initial release, with the SCell type and the `with_token` implementation

