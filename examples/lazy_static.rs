
use singleton_cell::SCell;
use singleton_cell::new_singleton;
use singleton_trait::Erased;
use std::error::Error;

pub fn main() -> Result<(), Box<dyn Error>> {
    let cell = SCell::new(vec![]);

    let (source1, source2) = (&cell, &cell);

    source1.borrow_mut(LOCK.write()?.borrow_mut()).push(0);

    println!("Look ma, I can read off {}",
        source2.borrow(LOCK.read()?.borrow())[0]);

    let write_access: Erased<&mut Lock> = LOCK.write()?.borrow_mut();
    let another_cell = SCell::new(0);

    another_cell.borrow(LOCK.read()?.borrow());

    Ok(())
}


new_singleton!(pub Lock);
/*
 *
 * Pretend you're using a well-known crate for generating
 * lazy static values.
 *
 * We lock up an Erased<Lock> for easy access to
 * the .borrow() and .borrow_mut() methods, but it's not
 * critical. These also help us trigger Deref/DerefMut for
 * the best inference
 *
 * You can also .erase() before storing in a data structure,
 * as any short-lived borrows should already be eliminated
 * by inlining.
 */
pub use lazy_static_holder::LOCK;
mod lazy_static_holder {
    use super::Lock;
    pub static LOCK: LazyLock =
        LazyLock::new();

    use std::sync::{RwLock, Once};
    use std::cell::UnsafeCell;
    use std::mem::MaybeUninit;
    use std::ops::Deref;
    use singleton_trait::Erased;
    pub struct LazyLock(UnsafeCell<MaybeUninit<RwLock<Erased<Lock>>>>);
    impl LazyLock {
        const fn new() -> Self {
            LazyLock(UnsafeCell::new(MaybeUninit::uninit()))
        }
    }
    unsafe impl Sync for LazyLock {}
    impl Deref for LazyLock {
        type Target = RwLock<Erased<Lock>>;
        fn deref(&self) -> &RwLock<Erased<Lock>> {
            static ONCE: Once = Once::new();
            ONCE.call_once(|| unsafe {
                let token = Lock::new().expect("created only once");
                // SAFETY: access is unique by Once
                *self.0.get() = MaybeUninit::new(RwLock::new(Erased::new(token)));
            });
            // SAFETY: this occurs after Once finishes initializing
            unsafe {
                &*(self.0.get() as *const RwLock<Erased<Lock>>)
            }
        }
    }
}
